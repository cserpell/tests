"""Little program to measure usage of memory when creating a dict."""
import guppy
import random

import debugger


def _one():
    """One random number."""
    return random.randint(0, 100)


def _size(heapy):
    """Current heap size."""
    return heapy.heap().size


def _print(i, heapy):
    """Print current status."""
    if (i % 10000) == 0:
        print '%s,%s' % (i, _size(heapy))


class DictMem:
    """Main program class."""

    def __init__(self, dict_size=0, file_name='test.txt'):
        """Constructor of DictMen.

        Args:
        dict_size: integer size of dict to generate.
        """
        self._dict_size = dict_size
        self._file_name = file_name

    def write_file(self):
        """Write file with random numbers to read dict."""
        with open(self._file_name, 'w') as output:
            for i in xrange(self._dict_size):
                output.write('%s\n' % _one())

    def using_for(self):
        """Build dict using for generating one random number at a time."""
        my_dict = {}
        heapy = guppy.hpy()
        for i in xrange(self._dict_size):
            my_dict[i] = _one()
            _print(i, heapy)

    def from_file(self):
        """Build dict updating it line by line from file."""
        my_dict = {}
        heapy = guppy.hpy()
        with open(self._file_name, 'r') as inp:
            for i in xrange(self._dict_size):
                my_dict[i] = int(inp.readline())
                _print(i, heapy)

    def from_file_exec(self):
        """Build dict updating it line by line from file using exec."""
        my_dict = {}
        heapy = guppy.hpy()
        with open(self._file_name, 'r') as inp:
            for i in xrange(self._dict_size):
                exec('my_dict[i] = int(inp.readline())')
                _print(i, heapy)

    def run(self):
        """Main program run point."""
        self.from_file_exec()


if __name__ == '__main__':
    main = DictMem(dict_size=1000000)
    main.run()

