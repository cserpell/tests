"""Test of concat strings speed."""


def concat_1(first, second):
  """Concat three strings the wrong way and return."""
  return first + 'test' + second


def concat_2(first, second):
  """Concat three strings the right way and return."""
  return '%stest%s' % (first, second)


def concat_3(first, second):
  """Concat three strings the right way and return."""
  return ''.join([first, 'test', second])


def main():
  """Main execution point."""
  last_str = ''
  for i in range(10000000):
    new_str = str(i)
    concat_1(last_str, new_str)
    last_str = new_str


if __name__ == '__main__':
  main()

