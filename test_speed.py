import datetime
import random


def gen_list(number):
  return [random.random() for a in xrange(number)]


def test_1(numbers):
  with open('test1.txt', 'wb') as out_file:
    out_file.write('\n'.join(str(a) for a in numbers))


def test_2(numbers):
  b = [round(a) for a in numbers]
  with open('test2.txt', 'wb') as out_file:
    out_file.write('\n'.join(str(a) for a in b))


def test_3(numbers):
  with open('test3.txt', 'wb') as out_file:
    out_file.write('\n'.join(str(round(a)) for a in numbers))


def main():
  nums = gen_list(1000000)
  d1 = datetime.datetime.utcnow()
  test_1(nums)
  d2 = datetime.datetime.utcnow()
  test_2(nums)
  d3 = datetime.datetime.utcnow()
  test_3(nums)
  d4 = datetime.datetime.utcnow()
  print ('first: %s' % (d2 - d1))
  print ('second: %s' % (d3 - d2))
  print ('third: %s' % (d4 - d3))


if __name__ == '__main__':
  main()
