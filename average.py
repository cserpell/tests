import random
import time


max_num = 10000
nums = 100

numbers = [random.randint(0, max_num) for i in xrange(nums)]

s0 = time.time()
avg = sum(numbers) / float(nums)

way_1 = sum((numbers[i] - avg) ** 2 for i in xrange(nums))

s1 = time.time()

way_2 = sum(numbers[i] * (numbers[i] - numbers[j]) for i in xrange(nums) for j in xrange(i + 1, nums)) / nums * 2

s2 = time.time()
print '%s ... %s ' % (way_1, s1 - s0)
print '%s ... %s ' % (way_2, s2 - s1)
