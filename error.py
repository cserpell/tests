import random
import time


max_num = 10000
nums = 100

numbers = [float(random.randint(0, max_num)) for i in xrange(nums)]

s0 = time.time()
avg = float(sum(numbers) / float(nums))

way_1 = sum((numbers[i] - avg) ** 2 for i in xrange(nums))

s1 = time.time()

avgs_g = [0.] * nums
so_far_g = [0.] * nums

for i in xrange(1, nums):
  so_far_old = so_far_g[i - 1]
  av_old = avgs_g[i - 1]
  sm_dif = numbers[i] - av_old
  avgs_g[i] = av_old + sm_dif / float(i + 1)
  so_far_new = so_far_old + (sm_dif * sm_dif) * (i / float(i + 1))
  so_far_g[i] = so_far_new

way_2 = so_far_g[nums - 1]

s2 = time.time()
print '%s ... %s ' % (way_1, s1 - s0)
print '%s ... %s ' % (way_2, s2 - s1)
