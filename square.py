import time


N = 5000

s0 = time.time()

i = 0
for x in range(0, N):
  i += x * x

print i

s1 = time.time()

i = 0
for x in xrange(0, N):
  i += x ** 2

print i

s2 = time.time()

print 'T0: %s' % (s1 - s0)
print 'T1: %s' % (s2 - s1)

