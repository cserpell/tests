"""Super test."""
import debugger


class A(object):
    """Base class."""

    xx = ['a']

    def __init__(self, hola):
        """Constructor of A."""
        self._hola = hola

    def hola(self):
        """Returns initial value."""
        return self._hola

    @staticmethod
    def static_test():
        """Static test method."""
        print 'A'


class B(A):
    """Child of A."""

    xx = A.xx + ['b']

    def hola(self):
        """Returns initial value with a wrapper string."""
        return 'B: %s' % super(B, self).hola()

    def new_static_test(self):
        """Calls parent static test."""
        return '%s ... %s' % (self.hola(), super(B).static_test())


if __name__ == '__main__':
    b = B('aa')
    print b.hola()
    debugger.debugger()
    print b.new_static_test()

