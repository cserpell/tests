import debugger


class Error1(Exception):
    """Error1."""


class Error2(Exception):
    """Error2."""


try:
    raise Error1('aaa')
except Error1 as e1:
    print e1
    debugger.debugger()
    raise Error2('bbb') from e1

