import datetime
import random


def gen_list(number):
  return [random.random() for a in xrange(number)]


def f_1(num):
  res = 0
  if num > 0.5:
    res = 1
  return res


def f_2(num):
  res = 1 if num > 0.5 else 0
  return res


def main():
  nums = gen_list(10000000)
  d1 = datetime.datetime.utcnow()
  a = [f_1(n) for n in nums]
  d2 = datetime.datetime.utcnow()
  b = [f_2(n) for n in nums]
  d3 = datetime.datetime.utcnow()
  print ('first: %s' % (d2 - d1))
  print ('second: %s' % (d3 - d2))


if __name__ == '__main__':
  main()
